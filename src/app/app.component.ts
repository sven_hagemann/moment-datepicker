import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'moment-datepicker';

  myFormular: FormGroup;

  constructor(public fb: FormBuilder) {}

  ngOnInit(): void {
    this.createForm();
  }

  createForm() {
    this.myFormular = this.fb.group({
      dob: [''],
    });
  }

  onClick() {
    // Attention: here we set a date of Nullmeridian (called now UTC - before GMT) - in German we must add
    //    +1 hour in winter (UTC + 1) or
    //    +2 hour in sommer(MESZ or CEST) (UTC + 2)
    // The imput-element in view displays "2. Mai 2000" instead of "1. Mai 2000" - as desired.

    // The datetime in the DB should always be saved in UTC-0!!!
    // In client use "moment" to switch into the corresponding time zone. >> See app.module.ts
    this.myFormular.get('dob').setValue('2000-05-01T22:00:00.000Z', {
      onlyself: true,
    });
  }

  // Send to DB
  submitForm() {
    if (this.myFormular.valid) {
      window.confirm(`Sending formdata: ${JSON.stringify(this.myFormular.value)}`);
    }
  }
}
